// fetch polyfill
import 'whatwg-fetch';
// detect mime-type data of asset paths
import { lookup as mime } from 'mime-types';

// Factory function to create the asset manager
export default function createAssetManager(opts = {}) {
  'use strict';

  // Asset storage
  let cache = {};

  // Download queue
  let queue = [];

  return {
    queueDownload(path) {
      queue.push(path);
    },

    async downloadAll() {
      // Make sure we have something to download
      if (queue.length === 0) {
        return Promise.resolve();
      }

      return Promise.all(queue.map(function (path) {
        let mimeType = mime(path);
        let type;

        // Make sure we support the file type
        if (mimeType.includes('image')) {
          type = 'image';
        } else if (mimeType.includes('audio')) {
          type = 'audio';
        } else if (mimeType.includes('json')) {
          type = 'json';
        } else {
          return Promise.reject(Error('Can only load image, audio or json files.'));
        };

        return fetch(path)
        .then(function fetchSuccess (response) {
          // Look for a succesful status code.
          // fetch(...) will not fail without network error.
          // See https://github.com/github/fetch#caveats
          if (response.status >= 200 && response.status < 300) {
            if (type === 'image' || type === 'audio') {
              return response.blob();
            } else if (type === 'json') {
              return response.json();
            }
          } else {
            // Error out
            var err = Error(`Failed to fetch ${path}: ${response.statusText}`);
            err.response = response;
            throw err;
          }
        })
        .then(function (data) {
          let resource;

          // Detech asset type
          switch (type) {
            case 'image': {
              resource = new Image();
              resource.onload = function imageOnLoad() {
                if (opts.onProgress) {
                  opts.onProgress(path);
                }
              };

              resource.src = URL.createObjectURL(data);

              break;
            }

            case 'audio': {
              resource = new Audio();
              resource.onload = function audioOnLoad() {
                if (opts.onProgress) {
                  opts.onProgress(path);
                }
              };

              resource.src = URL.createObjectURL(data);

              break;
            }


            case 'json': {
              try {
                resource = data;

                if (opts.onProgress) {
                  opts.onProgress(path);
                }
              } catch (err) {
                throw err;
              }

              break;
            }
          }

          // Store in cache
          cache[path] = resource;
        })
        .catch(function fetchError (err) {
          if (opts.onError) {
            opts.onError(path, err);
          }
        });
      }));
    },

    get(path) {
      return cache[path];
    }
  }
}
