# Asset Manager

A simple promise based asset manager to load local resources.

# Running Examples

```shell
npm install
jspm install
# run some server you like
live-server
```

Now browse the examples folder.

# Roadmap

- Load images, audio and JSON to start.
