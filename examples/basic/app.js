import createAssetManager from '../../index';

const assetManager = createAssetManager({
  onProgress: onProgress,
  onError: onError
});

// Queue'em up!
assetManager.queueDownload('assets/bunny.png');
assetManager.queueDownload('assets/complete.wav');
assetManager.queueDownload('assets/sample.json');

// Need wrapper async function to use await
async function runner() {
  try {
    // Ah yes, this is nice!
    await assetManager.downloadAll();

    assetManager.get('assets/complete.wav').play();
    document.body.appendChild(assetManager.get('assets/bunny.png'));
    console.log(assetManager.get('assets/sample.json'));
  } catch (err) {
    console.error(err);
  }
}

// Start the show
runner();

function onProgress(path) {
  console.log(`${path} was successfully loaded.`);
}

function onError(path, err) {
  console.error(`Error loading ${path}`);
  console.error(err);
}
